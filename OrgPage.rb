#!/usr/bin/env ruby
# coding: utf-8

# This file is part of TssWiki
# Copyright (C) 2016-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'erb'


require_relative('./Page.rb')


module TssWiki
    class OrgPage < Page
        def initialize(
                items = {},
                categories = [],
                description = ''
            )
            super()

            #The following is a very basic implementation
            #which can generate a very very long index.html.
            #But it's good enough for the first versions of TsMediaPage.
            #Besides, it's pre-alpha software ;)

            #TODO: make the path configurable and split indexes to
            #several files if the file lists become very big.

            content_template = IO.read(
                File.expand_path('./themes/' +
                                 @config.theme +
                                 '/org_page.html.erb')
            )
            @content_erb = ERB.new(content_template)

            @items = items
            @categories = categories
            @description = description
        end


        def addItem(page = nil)
            if (page.is_a?(TssWiki::Page))
                first_letter = page.title[0]
                if (!@items[first_letter].is_a?(Hash))
                    @items[first_letter] = {}
                end
                @items[first_letter][page.title] = page
            end
        end


        def generatePage()
            @items = @items.sort()
            @content = @content_erb.result(binding)
            super()
        end
    end
end
