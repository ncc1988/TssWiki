#!/usr/bin/env ruby
# coding: utf-8

# This file is part of TssWiki
# Copyright (C) 2016-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'singleton'


module TssWiki

    #The version number of TssWiki
    TSSWIKI_VERSION = '0.1.0'

    LOCALE_PATH = File.join(File.dirname(__FILE__), 'locale')

    class Config
        include Singleton
        include GetText

        attr_accessor :source_path
        attr_accessor :output_path
        attr_accessor :website_title
        attr_accessor :lang
        attr_accessor :theme
        attr_accessor :layout
        attr_accessor :colour_scheme
        attr_accessor :date_string
        attr_accessor :global_license_text
        #The date when the configuration was last modified:
        attr_accessor :mtime

        def initialize(config_file_name = nil)
            #Load the default configuration.
            #If a configuration file is specified
            #load its settings after the default configuration.
            self.loadDefaultConfig()
            @mtime = Time.at(0)
            if (config_file_name)
                if (self.load(config_file_name))
                    @mtime = File.mtime(config_file_name)
                end
            end
        end


        def loadDefaultConfig()
            @source_path = Dir.getwd() + '/doc'
            @output_path = Dir.getwd() + '/doc-html'
            @website_title = 'TssWiki'
            @lang = GetText.locale.to_s()
            @theme = 'default'
            @layout = 'default'
            @colour_scheme = 'default'
            @global_license_text = 'The content is available under the license ' +
                            '<a href="http://creativecommons.org/licenses/by-sa/4.0">' +
                            'Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)' +
                            '</a>, unless otherwise noted.'
            @date_string = '%Y-%m-%d %H:%I %Z'
        end

        def load(file_name = nil)
            if (!file_name)
                puts(_('NOTE: No configuration file specified. Loading default configuration!'))
                self.loadDefaultConfig()
                return false
            end

            if (!File.exist?(file_name))
                puts(
                    sprintf(
                        _('Error while loading the configuration: File "%s" does not exist!'),
                        file_name
                    )
                )
                return false
            end

            file = File.new(file_name, 'r')
            if (!file)
                puts(
                    sprintf(
                        _('Error while loading the configuration: File "%s" cannot be opened!'),
                        file_name
                    )
                )
                return false
            end

            @mtime = File.mtime(file_name)

            #load each line of the file and check if it contains a
            #configuration option:

            while (line = file.gets())
                if (line.match(/^ *#/))
                    #Lines having a '#' as first non-whitespace character
                    #are a comment and thereby ignored.
                    next
                end

                line = line.split('=', 2)
                key = line[0]
                value = line[1].tr("\n", '')

                case key
                when "source_path"
                    @source_path = value
                when "output_path"
                    @output_path = value
                when "website_title"
                    @website_title = value
                when "lang"
                    @lang = value
                when "theme"
                    @theme = value
                when "layout"
                    @layout = value
                when "colour_scheme"
                    @colour_scheme = value
                when "date_string"
                    @date_string = value
                when "global_license_text"
                    @global_license_text = value
                end
            end

            file.close()
            return true
        end

        def save(file_name = nil)
            if (!file_name)
                puts(_('Error while saving the configuration: No file specified!'))
                return false
            end

            file = File.new(file_name, 'w')
            if (!file)
                puts(
                    sprintf(
                        _('Error while saving the configuration into file "%s"!'),
                        file_name
                    )
                )
                return false
            end

            file.puts('source_path=' + @source_path)
            file.puts('output_path=' + @output_path)
            file.puts('website_title=' + @website_title)
            file.puts('lang=' + @lang)
            file.puts('theme=' + @theme)
            file.puts('layout=' + @layout)
            file.puts('colour_scheme=' + @colour_scheme)
            file.puts('global_license_text=' + @global_license_text)
            file.puts('date_string=' + @date_string)

            file.close()
            return true
        end
    end
end
