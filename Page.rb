#!/usr/bin/env ruby
# coding: utf-8

# This file is part of TssWiki
# Copyright (C) 2016-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require('erb')
require('open3')


require_relative('./Config.rb')


module TssWiki

    ##
    # This is the base class for pages TssWiki generates.
    # It is available in ERB templates.
    class Page
        #Include GetText and bind translations:
        include GetText
        bindtextdomain('TssWiki', :path => TssWiki::LOCALE_PATH)

        attr_accessor :title, :categories, :author, :date, :content,
                      :output_file_path, :output_link

        def initialize(
                source_file_path = ''
            )
            if (File.exists?(source_file_path))
                @source_file_path = source_file_path
                @source = IO.read(@source_file_path)
                @date = File.mtime(source_file_path)
            else
                @source_file_path = ''
                @source = ''
            end

            @title = ''
            @categories = ''
            @author = ''
            @date = Time.new()
            @content = ''
            @output_file_path = ''
            @output_link = ''

            # We need the global configuration in here.
            @config = TssWiki::Config.instance()
            #The global license is the default:
            @license_text = @config.global_license_text

            #Create the base ERB template:
            base_template_file_path = File.expand_path(
                './themes/' +
                @config.theme +
                '/base.html.erb'
            )
            base_template_file = IO.read(base_template_file_path)
            @base_page_template = ERB.new(base_template_file)
        end


        def setSource(source = '')
            @source = source
        end


        def setSourceType(source_type = '')
            @source_type = source_type
        end


        ##
        # Converts markdown or mediawiki source files to HTML using pandoc.
        def generatePage()
            if ((@source_file_path != '') and (@output_file_path != ''))
                print(@source_file_path + ' -> ' + @output_file_path + ': ')
                full_output_file_path = @config.output_path + @output_file_path
                if (File.exists?(full_output_file_path))
                    #The output file alrady exists.
                    #Check if it is newer than the input file:
                    if (File.mtime(full_output_file_path) > File.mtime(@source_file_path))
                        #The output file is newer than the source file.
                        #We don't need to generate the output file
                        #if there are no changes.
                        puts(_('UP TO DATE'))
                        return true
                    end
                end
            elsif (@output_file_path != '')
                print(@output_file_path + ': ')
            else
                #We cannot generate the page without an output path!
                return false
            end

            #Backup the language set by the system, then generate the HTML code
            #in the language specified by the TssWiki configuration:
            sys_lang = GetText.locale.to_s()
            GetText.locale = @config.lang
            html = @base_page_template.result(binding)
            #Restore system language:
            GetText.locale = sys_lang

            #Write the HTML output to the output file:
            page_file = File.open(
                File.expand_path(@config.output_path) + @output_file_path,
                'w'
            )

            page_file.write(html)
            page_file.close()
            puts(_('DONE'))
        end
    end
end
