# About TssWiki

TssWiki is a tiny wiki page generator written in Ruby.
The name stands for "**T**iny **s**imple **s**tatic **Wiki** generator".

It aims to support common features other wiki engines offer
while remaining easy to use and fast.

Its only dependencies are Ruby (>= 2.3), ruby-gettext (for translations),
Pandoc (for Markdown/Mediawiki to HTML conversion) and a unix shell.


# Setup

## Install dependencies

The following dependencies are necessary for TssWiki to work:

- Ruby
- ruby-gettext
- Pandoc
- A common unix shell like bash or a similar shell.

On Devuan GNU/Linux, Debian GNU/Linux and related operating systems
the following apt-get command will install all the dependencies:

    apt-get install ruby ruby-gettext pandoc bash

While ruby-gettext is also available as ruby gem, you may first want
to check if your distribution has that package already ready for you
via the distribution's standard package manager. This way you avoid
dependency problems in case the ruby gem version of ruby-gettext
is too new for your system's ruby installation.


## Installation

To install TssWiki, you need to download the source code and place it
somewhere in your file system. After that, navigate to the source folder
and run TssWiki with the --create-missing-stuff option so that TssWiki
will create an output directory and a configuration file with the
default configuration in the source folder:

    ./tsswiki.rb --create-missing-stuff

## Configuration

The last command created the default configuration in the file TssWiki.config.
This file has a simple key=value syntax. The following options are available:

### source_path

The path to the source files of the wiki. The directory specified by this path
should contain markdown or mediawiki files that shall be converted to wiki pages.
Markdown files should end with ".md", mediawiki files with ".mediawiki".

### output_path

The path where the generated wiki shall be placed. This folder will contain
HTML files after TssWiki generated a wiki from the source files.

### website_title

The title of the wiki.

### theme

The theme to be used. The name must match the folder name of the theme,
otherwise TssWiki cannot find the files for the theme.

### layout

The layout of the theme to be used. If the theme supports more than one
layout, the layout file name can be specified here. The layout file name
must be layout_$NAME.css.

### colour_scheme

The colour scheme of the theme to be used. The file name must be colour_scheme_$NAME.css.

### global_license_text

The license information text that shall be displayed on each page, if the page
itself doesn't have other license information in its metadata block.

### date_string

(This parameter is currently unused)

The date format that shall be used for displaying date and time information
of pages.


## Writing pages

You may want to start with the main page of your wiki. This page must have the
file name "index.md" or "index.mediawiki". The real title of the index page
can be set using a metadata block inside the file. Metadata blocks start at the
begin of a line and look like this:

    {{@metadata:
    title=Main page
    }}

This metadata block sets the title of the main page to "Main page".


Now you probably want to link to categories and other pages from the main page.
TssWiki has its own markdown extension that lets you write wiki links in the
MediaWiki syntax. For example, if you wish to link to the page "Apples" from the
main page in a sentence, you could do it like this:

    I have a page about [[Apples]] in this wiki.


To link to a specific category, you can also use the syntax above. In addition,
you may specify the display name of the wiki link. In the following example
the link will point to "./Category:Fruits.html", but inside the text it will
just be named "Fruits category".

    More about fruits can be found in the [[Category:Fruits|Fruits category]].


### A word about Markdown and Mediawiki syntax

It is possible to write a wiki in both syntaxes so that one part of the source
files are written in Markdown while the other part is written in the MediaWiki
syntax. It is important to note that mixing the syntaxes in one file
is not possible. Files with the extension ".md" will be treated as markdown
files while files with the extension ".mediawiki" will be treated as
Mediawiki source files.

For simple pages, Markdown may provide enough features. If you need advanced
features like footnotes or fancy (coloured) tables, the Mediawiki syntax
may be better suited.


## Customising TssWiki

You are free to customise TssWiki in regard to its functionality and the themes
that are applied on the output. While the core of TssWiki is released under the
terms of the GNU General Public License, the default theme is available
under the MIT license, making it easy for you to build your own theme from it.
This also means that you don't have to provide the source code to your theme
if you are using TssWiki.

## Writing your own theme

TssWiki themes are composed of ERB templates and CSS files. The ERB templates
should contain HTML code with placeholders for ruby objects.

### ERB templates

The main ERB templates must have specific names in order to be recognised
by TssWiki. You can outsource some parts of the main templates into smaller
templates as long as the main templates have the following names:

#### base.html.erb

This is the base template that produces the surrounding HTML
for a page. It should include the complete HTML document head with tags
that include the CSS file, if your theme uses CSS. TssWiki generates
one CSS file names style.css which contains all CSS rules you have defined
in your theme's layout and colour scheme CSS files
(see the section CSS files below).


The following objects are available in the base.html.erb template:

- config: The TssWiki configuration. See config.rb for a complete list
  of attributes.
- author: The author of the page as string.
- cateogries: A list of categories where the page lies in.
- title: The page's title as string.
- content: The page's content. This is a string with rendered HTML
  from another template.
- date: A Time object with the date when the page was last updated.
- license_text: A string holding the text referring to the license
  under which the page is available.


#### org_page.html.erb

This is the template for the content of pages that display different
structures inside the wiki, like the glossary or the pages that are
placed inside a category.

In the "org page" template, you have access to the following objects:

- description: A description text for the org page. This can be used mainly
  for the description text for a category.
- categories: A list of categories. This is only used in the category
  overview page.
- items: A list of sorted items that shall be displayed on the page.
  The items list has a letter as key and a list of pages as value.
  This makes it easy to group items by their starting letter.
  The pages list contain page object. See Page.rb for a list of all
  attributes the Page class provides.


#### wiki_page.html.erb

This template outputs the rendered content of the wiki page.
A string object named "content" is the only object available in this
template. It holds the rendered HTML data for the wiki page.

### CSS files

Theme CSS code is split up into colour schemes and layouts. This allows
you to combine every layout you have defined with every colour scheme
your theme provides. The file names are important here, too.

Layout CSS files have to start with "layout_", followed by the layout
name and the file extension ".css". Colour schemes file names start with
"colour_scheme_", followed by the colour scheme name and end with the
file extension ".css".

The CSS file TssWiki outputs is called style.css. It is the combination
of the layout and colour scheme CSS files, where the colour scheme rules
are appended after the layout rules.
