{{@metadata:
title=TssWiki for companies
categories=TssWiki
}}

TssWiki is a easy solution for personal knowledge management of individuals or organisational units inside a company. Each individual or organisational unit can run an own TssWiki instance to store knowledge specific to them. No web servers are required to let them run their own TssWiki instances since the generated pages are static. This means they can be put on the company's network storage and accessed from there with a web browser.


## Examples for knowledge management

An employee can for example store his business activities in his personal TssWiki and categorise them by using the built-in category functionality of TssWiki and thereby gaining quick access to very specific information regarding a special activity. This can reduce situations where the details of a business activity are lost because they aren't remembered.

An organisational unit can easily publish their guidelines on the company's intranet so that individuals who are new to that organisational unit can adapt more quickly to the guidelines. Since each unit can have their own TssWiki instance, the amount of information in each of these instances is limited and thereby more easy to find compared to using one big wiki for all organisational units.
