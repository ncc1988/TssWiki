{{@metadata:
title=Technical details
categories=TssWiki; Technical
}}

## Overview

- Purpose: static wiki generator
- written in Ruby
- source page format: markdown with extensions
  (for wiki page links and media files) or mediawiki
- dependencies:
  - pandoc (for markdown/mediawiki to HTML conversion),
  - Ruby 2.x
- license: GPLv3+ (GNU General Public License v3 or later)

### Requirements for generating a wiki

- Satisfied dependencies
- working Ruby installation
- about TODO KiB of free space

### Requirements for viewing a generated wiki

- basic web server (must be able to serve files)


## Web pages

- demo instance: [https://der.moe/TssWiki/](https://der.moe/TssWiki/)
- source code web page: [https://gitlab.com/ncc1988/TssWiki](https://gitlab.com/ncc1988/TssWiki)
