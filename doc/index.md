{{@metadata:
title=TssWiki Main Page
}}

## Welcome to TssWiki!

TssWiki is a new approach for wiki software. It is a static web page generator especially made for building wikis.

__Minimalism is one of the main principles of TssWiki.__

Contrary to other wiki systems it doesn't require PHP, Python, MySQL
or other software often found in web software.

For page generation TssWiki relies only on Pandoc (for Markdown/Mediawiki
source to HTML conversion) and standard Ruby code.

To serve pages you generated with TssWiki you only need a very basic
web server that is able to server files. Nothing more is required for TssWiki.

To browse TssWiki generated pages you can use a text browser or a
graphical browser. No JavaScript is required since TssWiki doesn't make use
of it. Just modern HTML and CSS, that's all :)


You can find out more about the technical details and the requirements
of TssWiki [[Technical details|on the technical details page]].


## Who can benefit from using TssWiki?

This is how TssWiki can be useful for different types of persons
or organisations:

- [[TssWiki for activists]]
- [[TssWiki for private websites]]
- [[TssWiki for companies]]


## How to generate a wiki with tsswiki?

### Setup / Configuration

#### First run

If you run TssWiki the first time it will create a configuration folder
in $HOME/.config/TssWiki and then copy CSS files, page blocks
(headers, navigation and footers) and a web server configuration file in there.
It will also create a configuration file called TssWiki.config
in the configuration folder.

#### The configuration file(s)

This TssWiki.config file holds all important configuration variables
like source path, output path, selected TssWiki theme and the wiki's name.

The TssWiki.config file in $HOME/.config/TssWiki is ignored when there is
a TssWiki.config file in the current working directory. This is useful,
if you have multiple TssWiki instances wich each instance having
a different configuration.

The file format is a simple key=value format with one key-value pair
per line, so that it is easily editable with any basic text editor.

#### Themes

TODO


#### htaccess.file

This is the template for apache .htaccess files. When a wiki is generated,
this file will be put in the output folder. It may be useful for beautifying
URLs or redirecting to the index page when a non-esistant page is accessed.


### Writing pages

You need to put your source files (in markdown or mediawiki syntax)
in the source directory. After that you may call tsswiki.rb which
will generate a wiki for you in the output directory.

__Source files must end with .md or .mediawiki to be processed!__

#### Metadata

After you have written a markdown or mediawiki file you may also add metadata
like categories or a title. These metadata are stored in a metadata block
that should be placed at the top of the file. The metadata block starts
with a line starting with "{{@metadata:" and ends with a line containing
only the characters "}}". The syntax of the metadata block is a simple
key=value syntax. Example:

    {{@metadata:
    title=Hello World
    categories=Example; Another category; Yet another category;
    }}
