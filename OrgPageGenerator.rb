#!/usr/bin/env ruby
# coding: utf-8

# This file is part of TssWiki
# Copyright (C) 2016-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'singleton'
require 'thread'


require_relative('./Config.rb')
require_relative('./OrgPage.rb')


module TssWiki
    class OrgPageGenerator
        include Singleton
        include GetText
        bindtextdomain('TssWiki', :path => TssWiki::LOCALE_PATH)


        def initialize()
            @mutex = Mutex.new()
            @page_index = OrgPage.new()
            @categories = {}
            @config = TssWiki::Config.instance()

            #Set the localised title:
            sys_lang = GetText.locale.to_s()
            GetText.locale = @config.lang
            @page_index.title = _('Glossary')
            GetText.locale = sys_lang
            @page_index.output_file_path = '/Wiki:Glossary.html'
            @page_index.output_link = './Wiki:Glossary.html'
        end


        def insertPageIntoCategory(wiki_page = nil, category = nil)
            # Since this method is called from threads (indirectly)
            # we must control the access to the page lists:

            @mutex.synchronize() {
                #Extract the first letter of the file name.
                #The indexes for categories and the general index
                #are sorted by first letter if they become too long.
                title = wiki_page.title
                first_letter = title.strip()[0]

                if (@page_index.is_a?(TssWiki::OrgPage))
                    @page_index.addItem(wiki_page)
                end

                if (category != nil)
                    #Add the page to the category index:
                    if (!@categories[category].is_a?(TssWiki::OrgPage))
                        org_page = OrgPage.new()
                        org_page.title = category
                        org_page.output_file_path = (
                            '/Category:' + category + '.html'
                        )

                        org_page.output_link = (
                            './Category:' + category + '.html'
                        )

                        @categories[category] = org_page
                    end
                    @categories[category].addItem(wiki_page)
                end
            }
        end


        ##
        # This method adds a page to the internal lists of
        # the OrgPageGenerator.
        def addPage(wiki_page = nil)
            if (wiki_page == nil)
                #We need the wiki page!
                return false
            end

            if (!wiki_page.categories.empty?())
                wiki_page.categories.each { |category|
                    self.insertPageIntoCategory(
                        wiki_page,
                        category
                    )
                }
            else
                #No categories are defined for the page.
                #It will still be added using the nil category:
                self.insertPageIntoCategory(
                    wiki_page,
                    nil
                )
            end
        end


        def generatePages()
            #Generate the general index (with all pages) first.

            #The page index is sorted: Generate the glossary page:
            @page_index.generatePage()

            category_index = OrgPage.new()
            #Set the localised title:
            sys_lang = GetText.locale.to_s()
            GetText.locale = @config.lang
            category_index.title = _('Categories')
            GetText.locale = sys_lang
            category_index.output_file_path = '/Wiki:Categories.html'
            category_index.output_link = './Wiki:Categories.html'

            #Now we must generate each category:
            @categories.each { |cat_name, cat_page|
                cat_page.generatePage()
                category_index.addItem(cat_page)
            }

            #And finally we generate an index with all categories in it:
            category_index.generatePage()

            return true
        end
    end
end
