��    
      l      �       �   
   �      �                $     7  
   N  �  Y  "   �  O    
   d     o     {     �     �     �     �  �  �     f               	                
              Categories Categories: DONE Generated with TssWiki Last updated on %s Merging stylesheets... UP TO DATE Usage: tsswiki.rb [PARAMETERS]
The following parameters are defined:
--version		Prints the version of TssWiki.
-h, --help		Prints this help text.
--create-missing-stuff	Sets up the configuration directory and configuration files.
-j<n>			Number of threads for the wiki page generation.
			Example: -j2 will use 2 threads to generate the wiki.
			By default TssWiki uses one thread for wiki page generation.
 Your wiki "%s" has been generated! Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-30 20:23+0000
PO-Revision-Date: 2019-01-30 20:35+0000
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
Last-Translator: 
Language: nb_NO
 Kategorier Kategorier: FERDIG Generert med TssWiki Sist oppdatering om %s Fusjonerer stylesheets... AKTUELL Bruk: tsswiki.rb [PARAMETER]
Følgende parameter er definert:
--version		Gi ut versjon fra TssWiki.
-h, --help		Gi ut disse hjelpetekst.
--create-missing-stuff	Rett opp konfigurasjonsmapper og konfigurasjonsfiler.
-j<n>			Antall av threads for generering av wikisider.
			Eksempel: -j2 vil bruk 2 threads for generering av wiki.
			Standard er at TssWiki vil bruker en thread for generering av wikisider.
 Dine wiki "%s" vært generert! 