��          t      �         
             (     -     D     M     U  
   h  �  s  "     N  .     }     �     �     �  	   �     �     �     �  �  �     �           	           
                                      Categories Categories: DONE Generated with TssWiki Glossary Imprint Last updated on %s UP TO DATE Usage: tsswiki.rb [PARAMETERS]
The following parameters are defined:
--version		Prints the version of TssWiki.
-h, --help		Prints this help text.
--create-missing-stuff	Sets up the configuration directory and configuration files.
-j<n>			Number of threads for the wiki page generation.
			Example: -j2 will use 2 threads to generate the wiki.
			By default TssWiki uses one thread for wiki page generation.
 Your wiki "%s" has been generated! Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-30 20:10+0000
PO-Revision-Date: 2019-01-30 20:23+0000
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
Last-Translator: 
Language: fr_FR
 Catégories Catégories: PRÊT Generé avec TssWiki Glossaire Mentions légales Dernière actualisation à %s À JOUR Usage: tsswiki.rb [PARAMÈTRES]
Des paramètres suivantes sont defini:
--version		Affiche la version de TssWiki.
-h, --help		Affiche cette texte d'aide.
--create-missing-stuff	Créer le répertioure de configuration et les fichiers de configuration.
-j<n>			Le nombre du threads pour la generation des pages du wiki.
			Exemple: -j2 va utiliser 2 threads pour generer le wiki.
			Par défault Tsswiki utilise un thread pour la generation des pages du wiki.
 Ton wiki "%s" a été generé. 