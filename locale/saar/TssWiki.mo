��          �   %   �      p  
   q     |     �  ,   �  %   �  B   �  @   #  4   d  8   �  '   �  '   �  5   "  )   X     �     �     �     �     �  E   �  ^     /   y     �  �   �  
   {  �  �  D     "   c  L  �  	   �	  
   �	     �	  0   �	  ,   !
  L   N
  B   �
  :   �
  =     '   W  %     =   �  /   �          )  	   1      ;     \  R   y  ]   �  .   *     Y  �   l     A  �  I  C         ^                                              	                                                                   
          Categories Categories: DONE ERROR: Cannot find colour scheme stylesheet! ERROR: Cannot find layout stylesheet! Error while loading the configuration: File "%s" cannot be opened! Error while loading the configuration: File "%s" does not exist! Error while saving the configuration into file "%s"! Error while saving the configuration: No file specified! Error: Output path (%s) does not exist! Error: Source path (%s) does not exist! Error: The number of threads cannot be less than one! Error: The output path cannot be created! Generated with TssWiki Glossary Imprint Last updated on %s Merging stylesheets... NOTE: No configuration file specified. Loading default configuration! The default configuration will be written to the file TssWiki.config in the current directory. The following output directory will be created: TssWiki Version %s TssWiki cannot load the configuration file. To create a configuration file, start TssWiki with the parameter --create-missing stuff. Use the --help parameter to see all available parameters. UP TO DATE Usage: tsswiki.rb [PARAMETERS]
The following parameters are defined:
--version		Prints the version of TssWiki.
-h, --help		Prints this help text.
--create-missing-stuff	Sets up the configuration directory and configuration files.
-j<n>			Number of threads for the wiki page generation.
			Example: -j2 will use 2 threads to generate the wiki.
			By default TssWiki uses one thread for wiki page generation.
 Your source directory (%s) is empty. Nothing to be done for TssWiki. Your wiki "%s" has been generated! Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-30 20:37+0000
PO-Revision-Date: 2019-01-30 20:47+0000
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
Last-Translator: 
Language: de
 Kategorie Kategorie: FERDICH FEHLER: Kann et Faawschema-Stylesheet nit finne! FEHLER: Kann et Layout-Stylesheet nit finne! Fehler beim Laden der Konfiguration: Die Datei "%s" kann nit geöffnet ginn! Fehler beim Laden der Konfiguration: Die Datei "%s" existiert nit! Fehler beim Speichern der Konfiguration in die Datei "%s"! Fehler beim Speichern der Konfiguration: Kää Datei aanginn! Fehler: Ausgabepfad (%s) existiert nit! Fehler: Quellpfad (%s) existiert nit! Fehler: Die Anzahl an Threads kann nit klääna als ens sinn! Fehler: Der Ausgabepfad kann nit erstellt ginn! Generierd mid TssWiki Glossar Impressum Letschd móó aktualisiert am %s Füje Stylesheets zesamme... HINWEIS: Kää Konfigurationsdatei aanginn. Die Standardkonfiguration wird gelaad! Die Standardkonfiguration wird in die Datei TssWiki.config im aktuelle Verzeichnis geschriew. Dat folgende Ausgabeverzeichnis wird erstellt: TssWiki Version %s TssWiki kann die Konfigurationsdatei nit lade. Um è Konfigurationdatei ze erstelle staatschde TssWiki middem Parameter --create-missing-stuff. Um all die verfügbaren Parameter ze siehn nutz de --help Parameter. AKTUELL Benutzung: tsswiki.rb [PARAMETER]
Die folgene Parameter sinn definiert:
--version		Zeicht die Version von TssWiki aan.
-h, --help		Zeicht diesen Hilfetext aan.
--create-missing-stuff	Leet et Konfigurationsverzeichnis unn die Konfigurationsdateie aan.
-j<n>			Die Anzahl an Threads für de Generierung vùn Wikiseite.
			Beispill: -j2 wird 2 Threads benutze um et Wiki ze generiere.
			Standardmäßig nutzt TssWiki ään Thread für de Generierung vùn Wikiseite.
 Dei Quellverzeichnis (%s) is leer. Für TssWiki givtet nix ze duun. Dei Wiki "%s" is generiert ginn! 