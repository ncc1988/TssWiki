��          �   %   �      p  
   q     |     �  ,   �  %   �  B   �  @   #  4   d  8   �  '   �  '   �  5   "  )   X     �     �     �     �     �  E   �  ^     /   y     �  �   �  
   {  �  �  D     "   c  �  �  
   
      
     ,
  =   3
  9   q
  P   �
  D   �
  :   A  ?   |  )   �  '   �  @     2   O     �     �  	   �     �     �  U   �  `   3  -   �     �  �   �     �  �  �  H   �     �                                              	                                                                   
          Categories Categories: DONE ERROR: Cannot find colour scheme stylesheet! ERROR: Cannot find layout stylesheet! Error while loading the configuration: File "%s" cannot be opened! Error while loading the configuration: File "%s" does not exist! Error while saving the configuration into file "%s"! Error while saving the configuration: No file specified! Error: Output path (%s) does not exist! Error: Source path (%s) does not exist! Error: The number of threads cannot be less than one! Error: The output path cannot be created! Generated with TssWiki Glossary Imprint Last updated on %s Merging stylesheets... NOTE: No configuration file specified. Loading default configuration! The default configuration will be written to the file TssWiki.config in the current directory. The following output directory will be created: TssWiki Version %s TssWiki cannot load the configuration file. To create a configuration file, start TssWiki with the parameter --create-missing stuff. Use the --help parameter to see all available parameters. UP TO DATE Usage: tsswiki.rb [PARAMETERS]
The following parameters are defined:
--version		Prints the version of TssWiki.
-h, --help		Prints this help text.
--create-missing-stuff	Sets up the configuration directory and configuration files.
-j<n>			Number of threads for the wiki page generation.
			Example: -j2 will use 2 threads to generate the wiki.
			By default TssWiki uses one thread for wiki page generation.
 Your source directory (%s) is empty. Nothing to be done for TssWiki. Your wiki "%s" has been generated! Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-30 20:09+0000
PO-Revision-Date: 2019-01-30 20:10+0000
Last-Translator: 
Language-Team: Moritz Strohm <ncc1988@posteo.de>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
 Kategorien Kategorien: FERTIG FEHLER: Das Farbschema-Stylesheet kann nicht gefunden werden! FEHLER: Das Layout-Stylesheet kann nicht gefunden werden! Fehler beim Laden der Konfiguration: Die Datei "%s" kann nicht geöffnet werden! Fehler beim Laden der Konfiguration: Die Datei "%s" existiert nicht! Fehler beim Speichern der Konfiguration in die Datei "%s"! Fehler beim Speichern der Konfiguration: Keine Datei angegeben! Fehler: Ausgabepfad (%s) existiert nicht! Fehler: Quellpfad (%s) existiert nicht! Fehler: Die Anzahl der Threads darf nicht weniger als eins sein! Fehler: Der Ausgabepfad kann nicht erzeugt werden! Erzeugt mit TssWiki Glossar Impressum Zuletzt aktualisiert am %s Kombiniere Stylesheets... HINWEIS: Keine Konfigurationsdatei angegeben. Die Standardkonfiguration wird geladen! Die Standardkonfiguration wird in die Datei TssWiki.config im aktuellen Verzeichnis geschrieben. Das folgende Ausgabeverzeichnis wird erzeugt: TssWiki Version %s TssWiki kann die Konfigurationsdatei nicht laden. Um eine Konfigurationsdatei zu erzeugen, starte TssWiki mit dem Parameter --create-missing-stuff. Der Parameter --help zeigt alle verfügbaren Parameter an. AKTUELL Benutzung: tsswiki.rb [PARAMETER]
Die folgenden Parameter sind definiert:
--version		Gibt die Version von TssWiki aus.
-h, --help		Gibt diesen Hilfetext aus.
--create-missing-stuff	Erzeugt das Konfigurationsverzeichnis und Konfigurationsdateien.
-j<n>			Die Anzahl an Threads für die Erzeugung von Wikiseiten.
			Beispiel: Bei -j2 werden zwei Threads für die Erzeugung des Wikis verwendet.
			Standardmäßig verwendet TssWiki einen Thread zur Erzeugung von Wikiseiten.
 Dein Quellverzeichnis (%s) ist leer. Für TssWiki gibt es nichts zu tun. Dein Wiki "%s" wurde erzeugt! 