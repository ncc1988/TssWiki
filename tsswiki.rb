#!/usr/bin/env ruby
# coding: utf-8

# This file is part of TssWiki
# Copyright (C) 2016-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require 'fileutils'
require 'gettext'


require_relative('./Config.rb')
require_relative('./OrgPageGenerator.rb')
require_relative('./WikiPage.rb')


module TssWiki

    ##
    # The main class of TssWiki.
    class Main
        #Include GetText and bind translations:
        include GetText
        bindtextdomain('TssWiki', :path => TssWiki::LOCALE_PATH)

        def self.printHelp()
            puts(
                _("Usage: tsswiki.rb [PARAMETERS]\n" +
                  "The following parameters are defined:\n" +
                  "--version\t\tPrints the version of TssWiki.\n" +
                  "-h, --help\t\tPrints this help text.\n" +
                  "--create-missing-stuff\tSets up the configuration directory and configuration files.\n" +
                  "-j<n>\t\t\tNumber of threads for the wiki page generation.\n" +
                  "\t\t\tExample: -j2 will use 2 threads to generate the wiki.\n" +
                  "\t\t\tBy default TssWiki uses one thread for wiki page generation.\n"
                 )
            )
        end


        def self.printVersion()
            puts(
                sprintf(
                    _('TssWiki Version %s'),
                    TssWiki::TSSWIKI_VERSION
                )
            )
            puts('Copyright 2016-2021 Moritz Strohm')
        end


        ##
        # This function does the main task of TssWiki:
        # Converting a source file to a wiki page
        def self.processSourceFile(file_name)
            #TODO: check if the file is readable

            #Init the converter:
            wiki_page = TssWiki::WikiPage.new(file_name)

            #Convert the wiki page:
            wiki_page.processSource()
            wiki_page.generatePage()
        end


        ##
        # The main function.
        def self.main()

            #The default is to use one thread:
            num_threads = 1
            #if missing folders/files etc. shall be created:
            create_missing_stuff = false

            #Handle command line arguments first:
            if (ARGV.length)
                #At least one argument is given:
                ARGV.each { |argument|
                    if (argument.start_with?('-j'))
                        #The amount of jobs (threads) is specified.
                        num_threads = argument.slice(/[0-9]+/).to_i()
                        if (num_threads < 1)
                            puts(_('Error: The number of threads cannot be less than one!'))
                            exit 1
                        end
                        puts(
                            sprintf(
                                _('The number of threads has been set to %d.'),
                                num_threads
                            )
                        )

                    elsif ((argument == '-h') or (argument == '--help'))
                        self.printHelp()
                        exit 0
                    elsif ((argument == '--create-missing-stuff'))
                        create_missing_stuff = true
                        puts(
                            _('The default configuration will be written to the ' +
                              'file TssWiki.config in the current directory.'
                             )
                        )
                        config = TssWiki::Config.instance()
                        config.save('./TssWiki.config')
                        if (!Dir.exist?(File.expand_path(config.output_path)))
                            puts(_('The following output directory will be created:'))
                            puts File.expand_path(config.output_path)
                            dir = Dir.mkdir(File.expand_path(config.output_path))
                            if (!dir)
                                puts(_('Error: The output path cannot be created!'))
                                exit 1
                            end
                        end
                        exit 0
                    elsif (argument == '--version')
                        printVersion()
                        exit 0
                    end
                }
            end

            #Load the configuration. If it cannot be found
            #in the current directory load it from the .config
            #directory of the current user. If the configuration
            #cannot be loaded from there, save the default
            #configuration in the current directory.
            config = TssWiki::Config.instance()
            if (!config.load('./TssWiki.config'))
                if (!config.load('~/.config/TssWiki.config'))
                    puts(
                        _('TssWiki cannot load the configuration file. ' +
                          'To create a configuration file, start TssWiki ' +
                          'with the parameter --create-missing stuff. ' +
                          'Use the --help parameter to see all available parameters.'
                         )
                    )
                    exit 1
                end
            end

            if (!Dir.exist?(File.expand_path(config.source_path)))
                puts(
                    sprintf(
                        _('Error: Source path (%s) does not exist!'),
                        File.expand_path(config.source_path)
                    )
                )
                exit 1
            end

            if (!Dir.exist?(File.expand_path(config.output_path)))
                puts(
                    sprintf(
                        _('Error: Output path (%s) does not exist!'),
                        File.expand_path(config.output_path)
                    )
                )
                exit 1
            end

            #Get a list with all wiki source files in the source directory:
            unprocessed_files = Dir.glob(
                File.expand_path(config.source_path) +
                '/**/*\.{md,mediawiki}'
            )

            if (unprocessed_files.empty?())
                puts(
                    sprintf(
                        _('Your source directory (%s) is empty. Nothing to be done for TssWiki.'),
                        File.expand_path(config.source_path)
                    )
                )
                exit 0
            end

            todo_lists = []
            #Split file list so that each thread has its own
            #separate file list:
            i = 0
            unprocessed_files.each { |item|
                if (!todo_lists[i % num_threads].is_a?(Array))
                    todo_lists[i % num_threads] = []
                end

                todo_lists[i % num_threads] << item
                i += 1
            }

            #Start the threads:
            threads = []
            num_threads.times do |i|
                thread_todo_list = todo_lists[i]
                if (thread_todo_list)
                    threads << Thread.new {
                        thread_todo_list.each { |file_name|
                            self.processSourceFile(file_name)
                        }
                    }
                end
            end

            #Wait until all threads have finished:
            threads.each { |thread|
                thread.join
            }

            #Now we generate the index and category pages:
            org_page_generator = TssWiki::OrgPageGenerator.instance()
            org_page_generator.generatePages()

            #Finally we must merge the stylesheets of the theme:
            css_file_path = '/themes/' + config.theme  + '/'

            layout_file_path = css_file_path + 'layout_' + config.layout + '.css'
            colour_scheme_file_path = css_file_path + + 'colour_scheme_' +
                                      config.colour_scheme + '.css'

            print(_('Merging stylesheets...'))

            if (File.exists?('.' + layout_file_path))
                #The css file is in the themes folder of the current directory.
                layout_file_path = '.' + layout_file_path
            elsif(File.exists?('~/.config/TssWiki/' + layout_file_path))
                #The css file is in the config folder in the home directory:
                layout_file_path = '~/.config/TssWiki/' + layout_file_path
            else
                puts(_('ERROR: Cannot find layout stylesheet!'))
                exit(1)
            end
            if (File.exists?('.' + colour_scheme_file_path))
                #The css file is in the themes folder of the current directory.
                colour_scheme_file_path = '.' + colour_scheme_file_path
            elsif(File.exists?('~/.config/TssWiki/' + colour_scheme_file_path))
                #The css file is in the config folder in the home directory:
                colour_scheme_file_path = '~/.config/TssWiki/' + colour_scheme_file_path
            else
                puts(_('ERROR: Cannot find colour scheme stylesheet!'))
                exit(1)
            end

            output_css_file_path = config.output_path + '/style.css'

            merge_css = false
            if (!File.exist?(output_css_file_path))
                merge_css = true
            elsif ((File.mtime(layout_file_path) > File.mtime(output_css_file_path)) or
                   (File.mtime(colour_scheme_file_path) > File.mtime(output_css_file_path))
                  )
                #The layout or colour scheme CSS source files are newer than the
                #combined CSS output file.
                merge_css = true
            elsif (config.mtime > File.mtime(output_css_file_path))
                #The configuration has changed. The CSS file should be generated.
                merge_css = true
            end

            if (merge_css)
                #We start by copying the layout file:
                FileUtils.cp(layout_file_path, output_css_file_path)
                #Now we append the colour scheme file:
                output_file = File.new(output_css_file_path, 'a')
                colour_scheme_file = File.new(colour_scheme_file_path, 'r')
                content = colour_scheme_file.read()
                output_file.write(content)
            end

            puts(_('DONE'))

            #Finished!
            puts(
                sprintf(
                    _('Your wiki "%s" has been generated!'),
                    config.website_title
                )
            )
        end
    end
end


#Call the main method of the main class:
TssWiki::Main.main()
