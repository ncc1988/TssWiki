#!/usr/bin/env ruby
# coding: utf-8

# This file is part of TssWiki
# Copyright (C) 2016-2021 Moritz Strohm <ncc1988@posteo.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


require('erb')
require('open3')
require('time')


require_relative('./Page.rb')


module TssWiki

    ##
    # This class represents a wiki page and its content.
    # It is used in the ERB template for wiki pages.
    class WikiPage < Page
        def initialize(
                source_file_path = ''
            )
            super(
                source_file_path
            )

            if (@source)
                source_file_extension = File.extname(@source_file_path).downcase()
                if (source_file_extension == '.md')
                    @source_type = 'markdown'
                elsif (source_file_extension == '.mediawiki')
                    @source_type = 'mediawiki'
                else
                    #HTML is assumed as fallback:
                    @source_type = 'html'
                end
            end
            #Create the wiki page ERB template:
            wiki_page_template_file_path = File.expand_path(
                './themes/' +
                @config.theme +
                '/wiki_page.html.erb'
            )

            wiki_page_template_path = IO.read(wiki_page_template_file_path)
            @wiki_page_template = ERB.new(wiki_page_template_path)
        end


        ##
        # This method is responsible for parsing metadata for a page
        # from a string.
        def parseMetadata(metadata_str = '')
            if (!metadata_str)
                return
            end

            metadata_str.lines.each do |line|
                line = line.split('=', 2)
                key = line[0]
                value = line[1]
                if (value)
                    #strip whitspace newlines etc.:
                    value = value.strip()
                end

                case key
                when 'title'
                    @title = value
                when 'author'
                    @author = value
                when 'license_text'
                    @license_text = value
                when 'date'
                    @date = Time.parse(value)
                when 'categories'
                    begin
                        categories = []
                        raw_category_list = value.split(';')
                        raw_category_list.each do |category|
                            categories << category.strip()
                        end

                        @categories = categories
                    end
                end
            end
        end


        ##
        # Processes the source data:
        # Strips metadata from source data and processes the metadata.
        def processSource()
            if (@source == '')
                return false
            end

            #Check for a metadata-block in the page content:
            metadata_start = @source.index(/^\{\{\@metadata\:/)
            if (metadata_start != nil)
                #metadata are present
                metadata_end = @source.index(/^\}\}$/, metadata_start)

                metadata_block_range = Range.new(metadata_start, metadata_end)
                metadata_block = @source[metadata_block_range]

                self.parseMetadata(metadata_block)

                #Remove the metadata block from @source.
                #We must substract 1 to metadata_start to filter out the string
                #'[[@metadata:' and add 2 to metadata_end to filter out the
                #string ']]'.
                source_before = ''
                if (metadata_start > 1)
                    source_before = @source[0..(metadata_start-1)]
                end
                source_after = @source[(metadata_end+2)..@source.length()]
                @source = source_before + source_after
            end
            if (@title == '')
                #No metadata could be read.
                #We must make up some metadata from the file name.
                @date = File.mtime(@source_file_path)
                @title = File.basename(
                    @source_file_path,
                    File.extname(@source_file_path)
                )
            end
        end


        ##
        # Converts custom markdown extensions to HTML.
        def convertSpecialMarkdown()
            new_source = ''

            #Replace wiki links: Collect everything enclosed by two
            #opening and two closing square brackets.

            input_offset = 0
            last_processed_offset = 0
            input_length = @source.length
            while (input_offset < input_length)
                #Metadata should already be stripped when this is called.
                #Otherwise the document may be broken!
                current_link_start = @source.index(
                    /\[\[/,
                    input_offset
                )
                if (current_link_start == nil)
                    #There are no links left in the input document.
                    new_source += @source.slice(
                        Range.new(input_offset, -1)
                    )
                    break
                end
                current_link_end = @source.index(']]', current_link_start)
                if (current_link_end == nil)
                    #Invalid link: Opening brackets but no closing
                    #brackets until the end of the document.
                    new_source += @source.slice(
                        Range.new(current_link_start, -1)
                    )
                    break
                end

                new_source += @source.slice(
                    Range.new(last_processed_offset, current_link_start-1)
                )
                link_text = @source.slice(
                    Range.new(current_link_start+2, current_link_end-1)
                )

                #We must check if the link is renamed.
                link_name = link_text
                link_text = link_text.split('|')
                if (link_text[1])
                    link_name = link_text[1]
                end

                #We have the link position and its text. Now we must
                #replace it with a valid markdown link:
                md_link = '[' + link_name + '](./' + link_text[0] + '.html "' +
                          link_name + '")'

                new_source += md_link

                last_processed_offset = current_link_end+2
                input_offset = current_link_end+2
            end

            @source = new_source
        end


        def convertPageSource()
            #Convert the source using pandoc:
            if (@source_type == 'markdown')
                #Replace wiki and image links which are a TssWiki specific
                #extension of markdown first.
                self.convertSpecialMarkdown()

                Open3.popen2('pandoc -f markdown -t html') {
                    |input, output, pandoc_thread|

                    input.write(@source)
                    input.close()
                    @content = output.read()
                }
            elsif (@source_type == 'mediawiki')
                Open3.popen2('pandoc -f mediawiki -t html') {
                    |input, output, pandoc_thread|

                    input.write(@source)
                    input.close()
                    @content = output.read()
                }
            else
                #Assume that the source is already HTML as fallback:
                @content = @source
            end
        end


        def addToCategories()
            cat_generator = TssWiki::OrgPageGenerator.instance()
            basename = File.basename(
                @source_file_path,
                File.extname(@source_file_path)
            )
            if (basename == 'index')
                #source file path is set to the special file basename "index".
                #If this is set, the output link is "./index.html"
                #instead of "./$page_title.html".
            else
                @output_link = './' + @title + '.html'
            end

            cat_generator.addPage(self)
        end


        ##
        # This method does the conversion of the page's content
        def generatePage()
            #First we generate the output file path:
            basename = File.basename(
                @source_file_path,
                File.extname(@source_file_path)
            )
            if (basename == 'index')
                #The index file is always named index.html:
                @output_file_path = '/index.html'
                @output_link = './index.html'
            else
                #In all other cases the file name is generated from the title:
                @output_file_path = '/' + @title + '.html'
                @output_link = './' + @title + '.html'
            end

            #Then we can generate the page:
            self.convertPageSource()
            if (@content)
                self.addToCategories()
                super()
            end
        end
    end
end
